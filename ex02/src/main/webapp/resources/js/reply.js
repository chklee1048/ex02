/**
 * 모듈 패턴 java의 클래스처럼 javascript를 이용해서 메서드를 가지는 객체를 구성 합니다. 
 * 모듈 패턴은 java의 즉시 실행 함수와 {}를 이용해서 객체를 구성합니다.
 *  
 * 즉시 실행 함수는 ()안에 함수를 선언하고 바깥쪽에서 실행해 버립니다.
 * 즉시 실행함수는 함수의 실행 결과가 바깥쪽에 선언된 변수에 할당됩니다. 
 * 위의 코드에서는 replyService라는 변수에 name이라는 속성에 'AAA'라는 속성값을 가진 객체가 할당 됩ㄴ다.
 * 
 * 외부에서는 repluService.add(객체, 콜백)를 전달하는 형태로 호출할 수 있는데, Ajax호출은 감춰져 있기 때문ㅇ ㅔ코드를 좀 더 깔끔하게 작성할 수 있습니다.
 */
console.log("reply module");

/*
       - 댓글 등록 -
       파라메터로 callback과 error를 함수로 받음.
        만일 ajax 호출이 성공하고 callback 값으로 적절한 함수가 존재한다면
        해당 함수를 호출해서 결과를 반영하는 방식 입니다.
 */
var replyService = (function() {
	
	function add(reply, callback, error){ // 파라메터로 callback과 error를 함수로 받음.
		// 만일 ajax 호출이 성공하고 callback 값으로 적절한 함수가 존재한다면
		// 해당 함수를 호출해서 결과를 반영하는 방식 입니다.
		console.log("add reply...........");
		
		$.ajax({
			type : 'post',
			url  : '/replies/new',
			data : JSON.stringify(reply),
			contentType : "application/json; charset=utf-8",
			success : function(result, status, xhr){
				if (callback){
					callback(result);
				}
			},
			error : function(xhr, status, er){
				if(error){
					error(er);
				}
			}
				
		});// end ajax
		
	}// end function add 
	
	/*
	 * 댓글 목록 처리
	 * getJson을 이용해서 처리할 수 있습니다.
	 * 
	 * getList는 param이라는 객체를 통해서 필요한 파라미터를 전달받아 JSON목록을 호출합니다. JSON형태가 필요하므로 URL호출 시 확장자를 '.json'으로 요구 합니다.
	 * 
	 */
	function getList(param, callback, error){
		var bno = param.bno;
		var page = param.page || 1;
		$.getJSON("/replies/pages/" + bno + "/" + page + ".json",
			function(data){
			if(callback){
				//callback(data); //댓글 목록만 가지고 오는 경우
				callback(data.replyCnt, data.list); //댓글 숫자와 목록을 가져오는 경우.
			}			
		}).fail(function(xhr, status, err){
			if(error){
				error();
			}
		});// end getJSON
	}// end function getList
	
	/*
	 * 댓글 삭제
	 */
	function remove(rno, callback, error){
		$.ajax({
			type: 'delete',
			url : '/replies/' + rno,
			success : function(deleteResult, status, xhr){
				if(callback){
					callback(deleteResult);
				}
			},
			error : function(xhr, status, er){
				if(error){
					error(er);
				}
			}
			
		});// end ajax
	}// end function remove
	
	function update(reply, callback, error){
		
		console.log("rno : "+reply.rno);
		
		$.ajax({
			type : 'put',
			url : '/replies/' +reply.rno,
			data: JSON.stringify(reply),
			contentType : "application/json; charset=utf-8",
			success : function(result, status, xhr){
				if(callback){
					callback(result);
				}
			}, error : function(xhr, status, er){
				if(error){
					eroor(er);
				}
			}
		}); // end ajax
		
	}// end function update
	
	function get(rno, callback, error){
		
		$.get("/replies/" + rno +".json", function(result){
			if(callback){
				callback(result);
			}			
		}).fail(function(xhr, status, err){
			if(error){
				error();
			}
		}); //end get
		
	}// end function get
	
	function displayTime(timeValue) {

		var today = new Date();

		var gap = today.getTime() - timeValue;

		var dateObj = new Date(timeValue);
		var str = "";

		if (gap < (1000 * 60 * 60 * 24)) {

			var hh = dateObj.getHours();
			var mi = dateObj.getMinutes();
			var ss = dateObj.getSeconds();

			return [ (hh > 9 ? '' : '0') + hh, ':', (mi > 9 ? '' : '0') + mi,
					':', (ss > 9 ? '' : '0') + ss ].join('');

		} else {
			var yy = dateObj.getFullYear();
			var mm = dateObj.getMonth() + 1; // getMonth() is zero-based
			var dd = dateObj.getDate();

			return [ yy, '/', (mm > 9 ? '' : '0') + mm, '/',
					(dd > 9 ? '' : '0') + dd ].join('');
		}
	}; // end function displaytime
	
	return {
				add		:add,
				getList : getList,
				remove  : remove,
				update  : update,
				get     : get,
				displayTime : displayTime
		   };
})();




/* 2차
var replyService = (function() {
	
	function add(reply, callback){
		console.log("reply...........");
	}
	
	return {name:"AAA"};
})();

*/

/* 1차 
 console.log("reply module");

var replyService = (function() {
	
	return {name:"AAA"};
})();

*/