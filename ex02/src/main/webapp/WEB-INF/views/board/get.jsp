<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@include file="../includes/header.jsp" %>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Board Read Page</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->	

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">

      <div class="panel-heading">Board Read Page</div>
      <!-- /.panel-heading -->
      <div class="panel-body">

		<div class="form-group">
			<label>No.</label> <input class="form-control" name='bno' value='<c:out value="${board.bno }" />' readonly="readonly">
		</div>

		<div class="form-group">
			<label>Title</label> <input class="form-control" name='title' value="<c:out value='${board.title} '/>" readonly="readonly">
		</div>       

		<div class="form-group">
            <label>Text area</label>
            <textarea class="form-control" rows="3" name='content' readonly="readonly"><c:out value='${board.content} '/></textarea>
		</div>

		<div class="form-group">
            <label>Writer</label> <input class="form-control" name='writer' value="<c:out value='${board.title} '/>" readonly="readonly">
		</div>
        
		<%-- <button data-oper='modify' class="btn btn-default"
		onclick="location.href='/board/modify?bno=<c:out value="${board.bno }" />'">modify</button>		
		<button data-oper='list' class="btn btn-info"
		onclick="location.href='/board/list' ">list</button> --%>
		<button data-oper='modify' class="btn btn-default">Modify</button>
		<button data-oper='list' class="btn btn-info">List</button>
		
		<form id="operForm" action="/board/modify" method="get">
			<input type="hidden" id='bno' name='bno' value=' <c:out value="${board.bno }"/> '/>
			<input type='hidden' name='pageNum' value='<c:out value="${cri.pageNum}"/>'>
  			<input type='hidden' name='amount' value='<c:out value="${cri.amount}"/>'>
		</form>
		

      </div>
      <!--  end panel-body -->

    </div>
    <!--  end panel-body -->
  </div>
  <!-- end panel -->
</div>
<!-- /.row -->


<!-- 댓글 영역 -->
<div class='row'>
  <div class="col-lg-12">
    <!-- /.panel -->
    <!-- <div class="panel panel-default">
		<div class="panel-heading">
        	<i class="fa fa-comments fa-fw"></i> Reply
        </div> -->
      <div class="panel-heading">
        <i class="fa fa-comments fa-fw"></i> Reply
        <button id='addReplyBtn' class='btn btn-primary btn-xs pull-right'>New Reply</button>
      </div>
      <!-- /end panel-heading -->
      <div class="panel-body">        
        <ul class="chat">
        	<!-- <li class="left clearfix" data-rno - '12'>
        	<div>
        		<div class="header">
        			<strong class="primary-font">user00</strong>
        			<small class="pull-right text-muted">2018-01-01 13:13</small>
        		</div>
        			<p>god job</p>
        		</div>
        	</li> -->
        </ul>
        <!-- ./ end ul -->
      </div>
      <!-- ./ end panel-body -->
		<div class="panel-footer">
		</div>
	</div>
	<!-- ./ end panel -->
  </div>
  <!-- ./ end col-lg-12 -->
</div>
<!-- ./ end row -->

<!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"
                aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">REPLY MODAL</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Reply</label> 
                <input class="form-control" name='reply' value='New Reply!!!!'>
              </div>      
              <div class="form-group">
                <label>Replyer</label> 
                <input class="form-control" name='replyer' value='replyer'>
              </div>
              <div class="form-group">
                <label>Reply Date</label> 
                <input class="form-control" name='replyDate' value='2018-01-01 13:13'>
              </div>
      
            </div>
<div class="modal-footer">
        <button id='modalModBtn' type="button" class="btn btn-warning">Modify</button>
        <button id='modalRemoveBtn' type="button" class="btn btn-danger">Remove</button>
        <button id='modalRegisterBtn' type="button" class="btn btn-primary">Register</button>
        <button id='modalCloseBtn' type="button" class="btn btn-default">Close</button>
      </div>          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

<script type="text/javascript" src="/resources/js/reply.js"></script>



<!-- 댓글 목록 javascript -->
<script>
	$(document).ready(function(){
		var bnoValue = '<c:out value="${board.bno}"/>';
		var replyUL = $(".chat");
		  
		showList(1);
		
		function showList(page){
			
			console.log("page : "+page);
					    
		    replyService.getList({bno:bnoValue,page: page|| 1 }, function(replyCnt, list) {
		    	
	    	console.log("replyCnt: "+ replyCnt );
	        console.log("list: " + list);
	        console.log(list);
	        
	        if(page == -1){
	          pageNum = Math.ceil(replyCnt/10.0);
	          showList(pageNum);
	          return;
	        }
		    	
			var str="";
		     
			if(list == null || list.length == 0){
				replyUL.html("");
				return;
		     }
		     
			for (var i = 0, len = list.length || 0; i < len; i++) {
			       str +="<li class='left clearfix' data-rno='"+list[i].rno+"'>";
			       str +="  <div><div class='header'><strong class='primary-font'>["
			    	   +list[i].rno+"] "+list[i].replyer+"</strong>"; 
			       str +="    <small class='pull-right text-muted'>"
			           +replyService.displayTime(list[i].replyDate)+"</small></div>";
			       str +="    <p>"+list[i].reply+"</p></div></li>";
		     }
		     
		     replyUL.html(str);
		     
		     showReplyPage(replyCnt);
		     
		   });
		     
		 }//end showList
		 
		 
//<div ckass='panel-footer'>에 댓글 페이지 번호를 출력하는 로직은 showReplyPage()는 아래와 같습니다.
		var pageNum = 1;
	    var replyPageFooter = $(".panel-footer");
	    
	    function showReplyPage(replyCnt){
	      
	      var endNum = Math.ceil(pageNum / 10.0) * 10;  
	      var startNum = endNum - 9; 
	      
	      var prev = startNum != 1;
	      var next = false;
	      
	      if(endNum * 10 >= replyCnt){
	        endNum = Math.ceil(replyCnt/10.0);
	      }
	      
	      if(endNum * 10 < replyCnt){
	        next = true;
	      }
	      
	      var str = "<ul class='pagination pull-right'>";
	      
	      if(prev){
	        str+= "<li class='page-item'><a class='page-link' href='"+(startNum -1)+"'>Previous</a></li>";
	      }
	      
	      for(var i = startNum ; i <= endNum; i++){
	        
	        var active = pageNum == i? "active":"";
	        
	        str+= "<li class='page-item "+active+" '><a class='page-link' href='"+i+"'>"+i+"</a></li>";
	      }
	      
	      if(next){
	        str+= "<li class='page-item'><a class='page-link' href='"+(endNum + 1)+"'>Next</a></li>";
	      }
	      
	      str += "</ul></div>";
	      
	      console.log(str);
	      
	      replyPageFooter.html(str);
	    }
		 
// 페이지 번호를 클릭했을 때 새로운 댓글을 가져오도록 하는 부분 입니다.		 
	    replyPageFooter.on("click","li a", function(e){
	        e.preventDefault();
	        console.log("page click");
	        
	        var targetPageNum = $(this).attr("href");
	        
	        console.log("targetPageNum: " + targetPageNum);
	        
	        pageNum = targetPageNum;
	        
	        showList(pageNum);
	      });
	    
		 
// 댓글 등록 버튼 클릭 시 모달 참 노출		
	    var modal = $(".modal");
	    var modalInputReply = modal.find("input[name='reply']");
	    var modalInputReplyer = modal.find("input[name='replyer']");
	    var modalInputReplyDate = modal.find("input[name='replyDate']");
	    
	    var modalModBtn = $("#modalModBtn");
	    var modalRemoveBtn = $("#modalRemoveBtn");
	    var modalRegisterBtn = $("#modalRegisterBtn");
	    
	    $("#modalCloseBtn").on("click", function(e){
	    	
	    	modal.modal('hide');
	    });
	    
	    $("#addReplyBtn").on("click", function(e){
	      
	      modal.find("input").val("");
	      modalInputReplyDate.closest("div").hide();
	      modal.find("button[id !='modalCloseBtn']").hide();
	      
	      modalRegisterBtn.show();
	      
	      $(".modal").modal("show");
	      
	    });
	    
// register 버튼 클릭 이벤트 처리
	    modalRegisterBtn.on("click",function(e){
	        
	        var reply = {
	              reply: modalInputReply.val(),
	              replyer:modalInputReplyer.val(),
	              bno:bnoValue
	            };
	        replyService.add(reply, function(result){
	          
	          alert(result);
	          
	          modal.find("input").val("");
	          modal.modal("hide");
	          
	          //showList(1);
	          showList(-1);
	          
	        });
	        
	      });
//댓글 조회 클릭 이벤트 처리 
	    $(".chat").on("click", "li", function(e){
	      
	      var rno = $(this).data("rno");
	      
	      replyService.get(rno, function(reply){
	      
	        modalInputReply.val(reply.reply);
	        modalInputReplyer.val(reply.replyer);
	        modalInputReplyDate.val(replyService.displayTime( reply.replyDate))
	        .attr("readonly","readonly");
	        modal.data("rno", reply.rno);
	        
	        modal.find("button[id !='modalCloseBtn']").hide();
	        modalModBtn.show();
	        modalRemoveBtn.show();
	        
	        $(".modal").modal("show");
	            
	      });
	    });

// 댓글 수정
	    modalModBtn.on("click", function(e){
	        
	        var reply = {rno:modal.data("rno"), reply: modalInputReply.val()};
	        
	        replyService.update(reply, function(result){
	              
	          alert(result);
	          modal.modal("hide");
	          showList(pageNum);
	          
	        });
	        
	      });

// 댓글 삭제
	      modalRemoveBtn.on("click", function (e){
	      	  
	    	  var rno = modal.data("rno");
	    	  
	    	  replyService.remove(rno, function(result){
	    	        
	    	      alert(result);
	    	      modal.modal("hide");
	    	      showList(pageNum);
	    	      
	    	  });
	    	  
	    	});

	    
		 
	});
</script>

<!-- 버튼 처리 javascript -->
<script>
	$(document).ready(function() {
		
		var operForm = $("#operForm");
		
		$("button[data-oper='modify']").on("click", function(e) {
			operForm.attr("action", "/board/modify").submit();
		});
		
		$("button[data-oper='list']").on("click", function(e) {
			operForm.attr("action", "/board/list");
			operForm.submit();
		});
	});
</script>

<script>
	$(document).ready(function() {
/*
	  - 댓글 입력 테스트 - 
	 		console.log("JS add test");
			var bnoValue = '<c:out value="${board.bno}"/>';
			//for replyService add test
			replyService.add(
				{reply:"JS test", replyer:"tester", bno:bnoValue}
				, function(result){
					alert("RESULT:" + result);
				}
			);
			console.log("JS add test");
*/
/* 
	 - 특정 게시글 댓글 조회 - 
			console.log("JS pages test");
			var bnoValue = '<c:out value="${board.bno}"/>';
			replyService.getList({bno:bnoValue, page:1}, function(list){
						for(var i = 0, len = list.length||0; i < len; i++){
							console.log(list[i]);
						}
			});
			console.log("JS pages test"); 
*/
/*	
	- 23번 댓글 삭제 -
			console.log("JS remove test");
			replyService.remove(23, function(count){
				console.log(count);
				if (count === "success"){
					alert("removed");
				}
					
				}, function(err){
					alert("error..");
			});
			console.log("JS remove test");
*/
/*
 	- 22번 댓글 수정-
		var bnoValue = '<c:out value="${board.bno}"/>';
		replyService.update({
			rno : 22,
			bno : bnoValue,
			reply : "Modified reply..."
		}, function(result){
			alert('수정 완료');
		});
*/
/*
	- 댓글 조회 -
		replyService.get(22,function(data){
			console.log(data);
		});
*/
	});// end document ready
</script>
<%@include file="../includes/footer.jsp" %>