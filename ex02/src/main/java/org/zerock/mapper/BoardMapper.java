package org.zerock.mapper;

import java.util.List;

import org.zerock.domain.BoardVO;
import org.zerock.domain.Criteria;

public interface BoardMapper {
	//@Select("SELECT * FROM tbl_board")
	public List<BoardVO> getList();		// 전체 리스트 검색
	
	public void insert(BoardVO board);	// 글 등록(등록되는 PK값 알 수 없음)
	
	public void insertSelectKey(BoardVO board);	// 글등록(등록되는 PK값 알 수 있음)
	
	public BoardVO read(Long bno);	// 특정 글 하나를 조회한다.
	
	public int delete(Long bno);	// 특정 글 하나를 삭제 합니다.
	
	public int update(BoardVO board);	// 특정 글 하나를 수정 합니다.
	
	public List<BoardVO> getListWithPaging(Criteria cri); // 검색 페이징
	
	public int getTotalCount(Criteria cri);
	
	
}
