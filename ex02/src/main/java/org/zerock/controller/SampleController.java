package org.zerock.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zerock.domain.SampleVO;
import org.zerock.domain.Ticket;

import lombok.extern.log4j.Log4j;

@RestController
@RequestMapping("/sample/*")
@Log4j
public class SampleController {

	// 기존은 리턴 값이 jsp페이지 이지만 이건 순수한 데이터가 됩니다.
	// header의 content-type에 담기는 것. 타입은 여러가지가 있다.
	// produce 속성은 해당 메서드가 생산하는 MIME 타입을 의미합니다.
	@GetMapping(value= "/getText", produces = "text/plain; charset=UTF-8")
	public String getText() {
		
		log.info("MIME TYPE : " + MediaType.TEXT_PLAIN_VALUE);
		
		return "안녕하세요";
	}
	
	// 객체의 반환
	// produce 항목 생략 가능
	@GetMapping(value="/getSample", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE
							   				   , MediaType.APPLICATION_XML_VALUE })
	public SampleVO getSample() {
		
		return new SampleVO(112, "스타","로드");
	}
	// http://localhost:8080/sample/getSample.json
	// {"mno":112,"firstName":"스타","lastName":"로드"}
	
//	Connection: keep-alive
//	Content-Type: application/json;charset=UTF-8
//	Date: Wed, 01 Jan 2020 03:05:08 GMT
//	Keep-Alive: timeout=20
//	Transfer-Encoding: chunked
	
	//컬렉션 타입의 객체 반환
	// getList는 내부적으로 1부터 10미만까지의 루프를 처리하면서 SampleVO 객체를 만들어서 List로 만들어 냅니다.
	// 배열이나 리스트 맵 타입의 객체들을 전송하는 경우
	@GetMapping(value = "/getList")
	public List<SampleVO> getList(){
		
		return IntStream.range(1, 10).mapToObj(i -> new SampleVO(i,i+ "First",i+" Last")).collect(Collectors.toList());
	}
	//http://localhost:8080/sample/getList.json
//	[
//		{"mno":1,"firstName":"1First","lastName":"1 Last"}
//		,{"mno":2,"firstName":"2First","lastName":"2 Last"}
//		,{"mno":3,"firstName":"3First","lastName":"3 Last"}
//		,{"mno":4,"firstName":"4First","lastName":"4 Last"}
//		,{"mno":5,"firstName":"5First","lastName":"5 Last"}
//		,{"mno":6,"firstName":"6First","lastName":"6 Last"}
//		,{"mno":7,"firstName":"7First","lastName":"7 Last"}
//		,{"mno":8,"firstName":"8First","lastName":"8 Last"}
//		,{"mno":9,"firstName":"9First","lastName":"9 Last"}
//	]
	
//	Connection: keep-alive
//	Content-Type: application/json;charset=UTF-8
//	Date: Wed, 01 Jan 2020 03:13:54 GMT
//	Keep-Alive: timeout=20
//	Transfer-Encoding: chunked
	
	// 맵의 경우는 키와 값을 가지는 하나의 객체로 간주
	// map을 이용하는 경우에는 키에 속하는 데이터는 XML로 반환되는 경우에 태그의 이름이 되기 때문에 문자열을 지정합니다.
	@GetMapping(value="/getMap")
	public Map<String, SampleVO> getMap(){
		
		Map<String, SampleVO> map = new HashMap<>();
		map.put("First", new SampleVO(111,"그루트","주니어"));
		
		return map;
	}
	
// 	http://localhost:8080/sample/getMap.json
//	{"First":{"mno":111,"firstName":"그루트","lastName":"주니어"}}
	
	//REST방식으로 호출하는 경우 화면 자체가 아니라 데이터 자체를 전송하는 방식으로 처리되기 때문에 
	// 데이터를 요청한 쪽에서는 정상적인 데이터인지 비정상적인 데이터인지를 구분할 수 있는 확실한 방법을 제공해야만 합니다.
	// ResponseEntity는 데이터와 함께 HTTP헤더의 상태 메시지 등을 같이 전달하는 용도로 사용합니다. 
	// HTTP 상태 코드오 ㅏ에러 메시지 등을 함께 데이터를 전달할 수있기 때문에 받는 입장에서는 확실할게 결과를 알 수있슷빈다.
	
	@GetMapping(value="/check", params = {"height", "weight"})
	public ResponseEntity<SampleVO> check(Double height, Double weight){
		
		SampleVO vo = new SampleVO(0, ""+height, ""+weight);
		
		ResponseEntity<SampleVO> result = null;
		
		if(height < 150) {
			result = ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(vo);
		} else {
			result = ResponseEntity.status(HttpStatus.OK).body(vo);
		}
		
		return result;
	}
	
// 	http://localhost:8080/sample/check.json?height=180&weight=148
// 	결과 네트워크 status가 200
	
// 	http://localhost:8080/sample/check.json?height=149&weight=148
// 	결과 네트워크 status가 502
	
	
	// RestController의 어노테이션
	// @PathVriable
	@GetMapping("/product/{cat}/{pid}")
	public String[] getPath(@PathVariable("cat") String cat, @PathVariable("pid") Integer pid) {
		
		return new String[] {"category: "+cat, "productid:"+pid};
	}
	
//  ttp://localhost:8080/sample/product/tete/61546541
//  ["category: tete","productid: 61546541"]
	
	// @RequestBody는 전달된 요청으 ㅣ내용을 이용해서 해당 파라미터의 타입으로 변환을 요구하빈다. 내부적으로는 HttpMessageConverter타입의 객체들ㅇ르 이용해서 다양한 포멧의 입력 데이터를 변환할 수 있습니다 
	// 대부부느으 ㅣ경우에는 JSON데이터를 서버에 보내서 원한느 타입의 객체로 변환하는 용도로 사용되지만 
	// 경우에 따라서는 원하는 포멧의 데이터를 보내고 이를 해석해서 원한느 타입을 ㅗ사용하기도 합니다.
	
	@PostMapping("/ticket")
	public Ticket convert(@RequestBody Ticket ticket) {
		log.info("convert ticket"+ticket);
		
		return ticket;
	}
	
	//@Rest방식의 테스트
	// API Tester로 
	// https: 아님. http
	// body영역에 {"tno":123,"owner":"qwer","grade":"asdf"}와 같이 원하는 내용을 전달할 수 있습니다.
	// 결과 = console 창에
	// INFO : org.zerock.controller.SampleController - convert ticketTicket(tno=123, owner=qwer, grade=asdf)
	
	// 다양한 전송 방식
	// REST 방식의 데이터 교환에서 가장 특이한 점은 기존으 ㅣGET/POST 외에 다양한 방식으로 데이터를 전달한다는 점이빈다. 
	// HTTP의 전송방식은 아래오 ㅏ같은 형태로 사용됩니다.
	// | 작업	  | 전송방식 |
	// |------|-------|
	// |Create|  POST |
	// | Read |  GET  |
	// |Update|  PUT  |
	// |Delete| Delete|
	// |------|-------|
	
	// REST(Representational State Transper)방식은 URI와 같이 전송방식을 결합 하므로,
	// 회원이라는 자원을 대상으로 전송방식을 다음과 같이 결합 합니다.
	// | 작업	  | 전송방식 | 	     URI     |
	// |------|-------|--------------| 
	// |  등록   |  POST | /members/new |
	// |  조회   |  GET  | /members/{id}|
	// |  수정   |  PUT  | /members/{id}+body (json 데이터 등)|
	// |  삭제   | Delete| /member/{id} |
	// |------|-------|--------------| 
	

}
