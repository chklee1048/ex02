package org.zerock.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zerock.domain.Criteria;
import org.zerock.domain.ReplyPageDTO;
import org.zerock.domain.ReplyVO;
import org.zerock.service.ReplyService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@RequestMapping("/replies/*")
@RestController
@Log4j
@AllArgsConstructor

public class ReplyController {
	
/*	ReplyController 설계
	 | 작업	| 전송방식 | 	     URI    |
	 |------|-------|---------------| 
	 |  등록   |  POST | /replies/new  |
	 |  조회   |  GET  | /replies/:rno |	 
	 |  삭제   | Delete| /replies/:rno |
	 |  수정   |  PUT  | /replies/:rno |
	 | 페이지  |  GET  | /replies/:rno/:page |
	 |------|-------|---------------|
	 
	 (1). REST 방식으로 동작하는 URL을 설계 할 때에는 PK를 기준으로 작성하는 것이 좋습니다.
	 (2). PK만으로 조회, 수정, 삭제가 가능하기 때문입니다.
	 (3). 브라우저나 외부에서 서버를 호출할 때, 외부에서 서버를 호출할 때 데이터의 포맷과 서버에서 보내주는 데이터의 타입을 명확히 설계해야 합니다.
	  	(예) 브라우저에서 JSON 전송하고, 서버(controller)에서 댓글의 처리 결과가 정상적을 ㅗ되었는지 문자열로 결과를 알려 주도록 합시다.
*/ 
	private ReplyService service;
	/*
	 * 글 등록
	 */
	@PostMapping(value="/new", consumes = "application/json", produces = {MediaType.TEXT_PLAIN_VALUE})
	public ResponseEntity<String> create(@RequestBody ReplyVO vo){
		// ResponseEntity는 데이터와 함께 HTTP헤더의 상태 메시지 등을 같이 전달하는 용도로 사용합니다. 
		// consumes와 produces를 이용해서 JSON 방식의 데이터만 처리하도록 하고, 문자열을 반환하도록 설계 합니다.
		// 파라미터 @RequestBody를 적용해서 JSON 데이터를 ReplyVO 타입으로 변환하도록 합니다. 
		log.info("ReplyVO : "+ vo);
		
		int insertCount = service.register(vo);
		
		log.info("reply insert count : " + insertCount);
		
		return insertCount == 1 ? new ResponseEntity<>("success", HttpStatus.OK)
				                : new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/*
	 * 특정 게시물의 댓글 목록 확인
	 */
	@GetMapping(value="/pages/{bno}/{page}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<ReplyPageDTO> getList(@PathVariable("page") int page, @PathVariable("bno") Long bno){
		
		log.info("get List");
		Criteria cri = new Criteria(page, 10);
		log.info("cri : "+ cri);
		
		service.getList(cri, bno);
		
		return new ResponseEntity<>(service.getListPage(cri, bno), HttpStatus.OK);
		
	}
	
	/*
	 * 댓글 수정
	 */
	@RequestMapping(value="/{rno}", method = {RequestMethod.PUT, RequestMethod.PATCH}, consumes = "application/json", produces = {MediaType.TEXT_PLAIN_VALUE})
	public ResponseEntity<String> modify(@RequestBody ReplyVO vo, @PathVariable("rno") Long rno){
		
		vo.setRno(rno);
		log.info("modify rno :"+rno);
		
		log.info("modify vo : "+ vo);
		
		return service.modify(vo) == 1 ? new ResponseEntity<>("success", HttpStatus.OK) 
				                       : new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
				
		
	}
	/*
	 * 댓글 삭제
	 */
	@DeleteMapping(value = "/{rno}", produces = { MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<String> remove(@PathVariable("rno") Long rno) {

		log.info("remove: " + rno);

		return service.remove(rno) == 1 
				? new ResponseEntity<>("success", HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	/*
	 * 댓글 조회 처리
	 */	
	@GetMapping(value = "/{rno}", 
			produces = { MediaType.APPLICATION_XML_VALUE, 
					     MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ReplyVO> get(@PathVariable("rno") Long rno) {

		log.info("get: " + rno);

		return new ResponseEntity<>(service.get(rno), HttpStatus.OK);
	}
	
}
