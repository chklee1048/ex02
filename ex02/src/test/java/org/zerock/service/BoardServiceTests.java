package org.zerock.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zerock.domain.BoardVO;
import org.zerock.domain.Criteria;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/root-context.xml")
// Java Config
// @ContextConfiguration(classes = {org.zerock.config.RootConfig.class} )
@Log4j

public class BoardServiceTests {
	
	@Setter(onMethod_ = {@Autowired})
	private BoardService service;
	
	@Test
	public void testExist() {
		String aa = null;
		
		log.info("1");
		log.info(service);
		log.info("2");
		assertNotNull(service); // service가 null이 아님을 확인한다.
		log.info("3");
	}
	
	@Test
	public void testRegister() {
		log.info("11");
		BoardVO board = new BoardVO();
		board.setTitle("새로 등록한 제목");
		board.setContent("새로 등록된 내용");
		board.setWriter("newbie");
		log.info("22");
		service.register(board);
		log.info("33");
		
	}
	
	@Test
	public void testGetList() {
		
		log.info("111");
		Criteria cri = new Criteria();
		cri.setAmount(10);
		cri.setPageNum(3);
		log.info(service.getList(cri));
		log.info("222");
	}
}
